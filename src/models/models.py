import os
from elasticsearch_dsl import Document, Text, Keyword, Integer, Boolean
from elasticsearch_dsl.connections import connections


class ElasticORMHouse(Document):
    uuid = Keyword()
    m2 = Integer()
    rooms = Integer()
    location = Text()
    zipcode = Text()
    price = Integer()
    predicted = Boolean()

    class Index:
        name = 'house'
        settings = {
          "number_of_shards": 2,
        }

    @classmethod
    def execute_and_serialize(cls, search):
        search.execute()
        serialized_matches = list()
        for hit in search:
            serialized_matches.append(hit.to_dict())
        return serialized_matches

    @classmethod
    def find_all(cls):
        search = cls.search()
        serialized_matches = cls.execute_and_serialize(search)
        return serialized_matches

    @classmethod
    def find_by_uuid(cls, uuid: str):
        search = cls.search().filter('term', uuid=uuid)
        serialized_matches = cls.execute_and_serialize(search)
        return serialized_matches

    @classmethod
    def delete_by_uuid(cls, uuid: str):
        search = cls.search().filter('term', uuid=uuid)
        serialized_matches = cls.execute_and_serialize(search)
        search.delete()
        return serialized_matches

    def update_by_uuid(self, uuid: str, **kwargs):
        search = self.search().filter('term', uuid=uuid)
        search.execute()
        for hit in search:
            # Switch instance reference to the found document one
            self.meta.id = hit.meta.id
            super(ElasticORMHouse, self).save(**kwargs)
            return True
        return False


class HouseFactory(object):
    DEFAULT_PERSISTENCE_BACKEND = 'elasticsearch'

    @classmethod
    def _create_elastic_house(cls):
        connections.create_connection(hosts=[
            {
                'host': os.getenv('ELASTICSEARCH_HOST', 'localhost'),
                'port': os.getenv('ELASTICSEARCH_PORT', '9200')
            }
        ])
        ElasticORMHouse.init()
        return ElasticORMHouse

    @classmethod
    def create(cls):
        persistence_backend = os.getenv('PERSISTENCE_BACKEND', cls.DEFAULT_PERSISTENCE_BACKEND).lower()
        if persistence_backend == 'elasticsearch':
            house = cls._create_elastic_house()
        else:
            raise Exception(f'Invalid persistance backend "{persistence_backend}"')
        return house
