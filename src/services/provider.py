from uuid import uuid4
from models.models import HouseFactory
from ml.house import predictive_model


class HouseService(object):
    def get_house(self) -> object:
        return HouseFactory.create()

    def fill_house(self, payload):
        house = self.get_house()
        if 'price' not in payload:
            # Predict price if not provided
            price = predictive_model.predict(payload['m2'])[0]
            predicted = True
        else:
            price = payload['price']
            predicted = False
        filled_house = house(
            uuid=payload['uuid'],
            m2=payload['m2'],
            rooms=payload['rooms'],
            location=payload['location'],
            zipcode=payload['zipcode'],
            price=price,
            predicted=predicted
        )
        return filled_house

    def save(self, payload: dict) -> (dict, bool):
        payload['uuid'] = uuid4()
        filled_house = self.fill_house(payload)
        saved = filled_house.save()
        return payload, saved

    def find_all(self) -> tuple:
        return self.get_house().find_all()

    def find_by_uuid(self, uuid: str) -> tuple:
        return self.get_house().find_by_uuid(uuid)

    def exists_by_uuid(self, uuid: str) -> bool:
        results = self.find_by_uuid(uuid)
        if results:
            return True
        return False

    def delete_by_uuid(self, uuid: str) -> tuple:
        results = self.get_house().delete_by_uuid(uuid)
        return results

    def update_by_uuid(self, payload: dict, uuid: str) -> (dict, bool):
        payload['uuid'] = uuid
        filled_house = self.fill_house(payload)
        return payload, filled_house.update_by_uuid(uuid)
