from injector import inject
from services.provider import HouseService


class House(object):
    @inject
    def get_all(self, house_service: HouseService) -> tuple:
        houses = house_service.find_all()
        if houses:
            return houses, 200
        else:
            return houses, 404

    @inject
    def create_idempotent(self, house_service: HouseService, house: dict) -> tuple:
        mapped_house, created = house_service.save(house)
        if created:
            return mapped_house, 201
        else:
            return mapped_house, 400

    @inject
    def create_idempotent_alias1(self, house_service: HouseService, house: dict) -> tuple:
        return self.create_idempotent(house_service, house)

    @inject
    def get_by_uuid(self, house_service: HouseService, uuid: str) -> tuple:
        house = house_service.find_by_uuid(uuid)
        if house:
            return house, 200
        return house, 404

    @inject
    def delete_by_uuid(self, house_service: HouseService, uuid: str) -> tuple:
        house = house_service.delete_by_uuid(uuid)
        if house:
            return house, 201
        return house, 404

    @inject
    def update_by_uuid(self, house_service: HouseService, uuid: str, house: dict) -> tuple:
        mapped_house, updated = house_service.update_by_uuid(house, uuid)
        if updated:
            return mapped_house, 201
        return mapped_house, 404


house_instance = House()
