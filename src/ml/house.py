from housemodel.model import HouseRentModelBuilder

predictive_model = HouseRentModelBuilder().get_model()
predictive_model.load_model()
