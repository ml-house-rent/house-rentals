# House rentals microservice
REST microservice for CRUD operations of house rentals with predictive prices functionality.

## Requirements
* You need Docker and doker-compose software.
* Ensure you have available the ports 8080, 9200 and 9300 in your machine or change them in the docker-compose.yaml file.

## Quickstart
Run the docker compose services with:

```docker-compose up -d```

Now, access to the following url in your browser:

[http://localhost:8080/ui/](http://localhost:8080/v1.0/ui/)

Now click on default and POST /house/. Then, in the house parameter textbox copy something like:
```json
{
  "m2": 75,
  "rooms": 3,
  "location": "Sagrada Familia",
  "zipcode": "08000",
  "price": 900
}
```
Price is optional, if it's not sent the service will predict it.

### Additional notes
Note that the id field will be autogenerated later. If everything runs OK, it will return an HTTP 201 code with the house object you've just created.

Now you can list the object in ElasticSearch, open in your browser:

[http://localhost:9200/house/_search?pretty=true&q=\*:\*](http://localhost:9200/house/_search?pretty=true&q=*:*)
